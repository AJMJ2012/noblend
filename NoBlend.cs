﻿using Terraria.ID;
using Terraria.ModLoader;
using Terraria;

namespace NoBlend {
	public class NoBlend : Mod {
		public NoBlend() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}

		public override void Load() {
			DALib.ConflictCheck.AddConflict(this, "AllBlend");
		}
		
		public override void PostSetupContent() {
			for (int index = 0; index < Main.tileTexture.Length; ++index) {
				Main.tileBlendAll[index] = false;
				Main.tileMergeDirt[index] = false;
				Main.tileBrick[index] = false;
				for (int index2 = 0; index2 < Main.tileTexture.Length; ++index2) {
					Main.tileMerge[index][index2] = false;
					Main.tileMerge[index2][index] = false;
				}
			}
		}
	}
}